import React from "react";
import { Pressable, Text } from "react-native";

function CancelButton({ onPress, title }) {
  return (
    <Pressable
      className="p-2 px-6 bg-red-500 rounded-xl shadow-md active:bg-red-600"
      onPress={onPress}
    >
      <Text
        style={{ fontFamily: "yekan" }}
        className="text-lg text-white text-center"
      >
        {title}
      </Text>
    </Pressable>
  );
}

export default CancelButton;
