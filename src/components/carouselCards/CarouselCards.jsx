import React from "react";
import { Dimensions, Image, Text, View } from "react-native";
import Carousel from "react-native-reanimated-carousel";
import imageData from "./imageData";

const CarouselCards = () => {
  const width = Dimensions.get("window").width;
  return (
    <>
      <View className='items-center'>
        <Carousel
          loop
          width={width}
          style={{
            width: "100%",
          }}
          mode="parallax"
          modeConfig={{
            parallaxScrollingScale: 0.9,
            parallaxScrollingOffset: 50,
          }}
          autoPlay={false}
          data={imageData}
          scrollAnimationDuration={1000}
          onSnapToItem={(index) => console.log("current index:", index)}
          renderItem={({ item, index }) => (
            <View key={index}>
              <Image className="w-full h-full" source={item.imgUrl} />
            </View>
          )}
        />
      </View>
    </>
  );
};

export default CarouselCards;
