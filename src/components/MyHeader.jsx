import React from "react";
import { Text, View } from "react-native";

function MyHeader({nameTitle}) {
  return (
    <View style={{ flexDirection: "row", margin: 15 }}>
      <Text style={{ fontWeight: "bold", fontSize: 30, color: "white" }}>
        {nameTitle}
      </Text>
    </View>
  );
}

export default MyHeader;
