import React from "react";
import { Pressable, Text } from "react-native";

function OKButton({ title, onPress }) {
  return (
    <Pressable
      className="p-2 px-6 bg-blue-500 rounded-xl shadow-md active:bg-blue-600"
      onPress={onPress}
    >
      <Text
        style={{ fontFamily: "yekan" }}
        className="text-lg text-white text-center"
      >
        {title}
      </Text>
    </Pressable>
  );
}

export default OKButton;
