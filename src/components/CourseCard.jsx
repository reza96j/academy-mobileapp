import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useEffect, useState } from "react";
import { Image, Pressable, Text, View } from "react-native";

function CourseCard({ price, teacher, course, navigation, cardID }) {
  const [cartItem, setCartItem] = useState([]);
  const [flag, setFlag] = useState(false);

  const loadCart = async () => {
    let getData = await AsyncStorage.getItem("cart");
    getData = JSON.parse(getData);
    if (getData) {
      setCartItem(getData);
    } else {
      let newCart = [];
      await AsyncStorage.setItem("cart", JSON.stringify(newCart));
    }
  };
  
  useEffect(() => {
    loadCart();
  }, [flag]);

  const addToCart = async () => {
    const buyProduct = {
      id: cardID,
      course: course,
      price: price,
      teacher: teacher,
    };
    let getData = await AsyncStorage.getItem("cart");
    getData = JSON.parse(getData);

    if (getData) {
      const newdata = [...getData, buyProduct];
      await AsyncStorage.setItem("cart", JSON.stringify(newdata));
      console.log("add :", newdata);
    }

    setFlag((old) => !old);
  };

  // const courseById = cartItem.filter((cf) => cf.userID === student._id);
  const showAdd = cartItem.some((Cs) => Cs.id === cardID) ? "hidden" : "";
  const showRemove = cartItem.some((Cs) => Cs.id === cardID) ? "" : "hidden";

  const removeFromCart = async () => {
    const newCartList = cartItem.filter((cart) => cart.id !== cardID);
    console.log("remove :", newCartList);
    await AsyncStorage.setItem("cart", JSON.stringify(newCartList));
    setFlag((old) => !old);
  };

  return (
    <View
      className="p-2 mx-1 mb-4 mt-3 shadow-md shadow-slate-800 rounded-xl flex-row-reverse bg-slate-50 dark:bg-slate-400
    "
    >
      <Pressable
        className="w-28 h-28 mt-1 rounded-full shadow-lg shadow-black"
        onPress={() => navigation.navigate("lesson")}
      >
        <Image
          source={require("../assets/images/UI-UX.jpg")}
          className="w-28 h-28 rounded-full"
        />
      </Pressable>
      <View className=" w-44 ">
        <View className="h-14 ">
          <Text
            className="text-xl mr-3 dark:text-white"
            style={{ fontFamily: "yekan" }}
          >
            {course}
          </Text>
        </View>
        <View className=" h-9 flex-row-reverse">
          <Image
            className="w-5 h-6 mr-3 opacity-70"
            source={require("../assets/images/icons/user.png")}
          />
          <Text
            className="text-sm mr-1 mt-1 text-zinc-500 dark:text-slate-50"
            style={{ fontFamily: "yekan" }}
          >
            {teacher}
          </Text>
        </View>
        <View className=" flex-row-reverse">
          <Image
            className="w-5 h-5 mr-3 opacity-70"
            source={require("../assets/images/icons/cash.png")}
          />
          <Text
            className="text-lg mr-1 text-red-700 dark:text-red-600"
            style={{ fontFamily: "yekan" }}
          >
            {price} تومان
          </Text>
        </View>
      </View>
      <View className="  items-center">
        <Image
          source={require("../assets/images/icons/favorite.png")}
          className="w-9 h-9 opacity-60"
        />
        <Pressable
          className={`${showAdd} h-12 w-12 left-1 top-1 rounded-full bg-emerald-500 shadow-md shadow-black active:bg-emerald-600 items-center justify-center mt-7`}
          onPress={addToCart}
        >
          <Text className="text-4xl text-white">+</Text>
        </Pressable>
        <Pressable
          className={`${showRemove} h-12 w-12 left-1 top-1 rounded-full bg-red-500 shadow-md shadow-black active:bg-red-600 items-center justify-center mt-7`}
          onPress={removeFromCart}
        >
          <Text className="text-3xl mb-1 text-white">x</Text>
        </Pressable>
      </View>
    </View>
  );
}

export default CourseCard;
