import { useParams } from "react-router-dom";
import Http from "../interceptor/interceptor";

const MainUrl = process.env.REACT_APP_PUBLIC_PATH;

const GetCourses = async () => {
  try {
    const result = await Http.get(`${MainUrl}course/getall`);
    return result.data.result;
  } catch (error) {
    return false;
  }
};

const GetCourse = async (id) => {
  try {
    const result = await Http.get(`${MainUrl}course/${id}`);

    return result.data.result;
  } catch (error) {
    return false;
  }
};

const GetStudentCourse = async (_id) => {
  try {
    const result = await Http.get(`${MainUrl}student/${_id}`);

    return result.data.result;
  } catch (error) {
    return false;
  }
};

export { GetCourses, GetCourse, GetStudentCourse };
