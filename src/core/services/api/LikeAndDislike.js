import Http from "../interceptor/interceptor";

// main url of backend
const MainUrl = process.env.REACT_APP_PUBLIC_PATH;

const LikeCourse = async (obj) => {
  try {
    const result = await Http.post(`${MainUrl}course/like`, obj);

    return result.data.message;
  } catch (error) {
    return false;
  }
};

const DislikeCourse = async (obj) => {
  try {
    const result = await Http.post(`${MainUrl}course/dislike`, obj);

    return result.data.message;
  } catch (error) {
    return false;
  }
};

const CountLikeCourse = async (_id) => {
  try {
    const result = await Http.get(`${MainUrl}course/likeCount/${_id}`);

    return result.data;
  } catch (error) {
    return false;
  }
};

export { DislikeCourse, LikeCourse, CountLikeCourse };
