import { useParams } from "react-router-dom";
import Http from "../interceptor/interceptor";

const MainUrl = process.env.REACT_APP_PUBLIC_PATH;

const GetNews = async () => {
  try {
    const result = await Http.get(`${MainUrl}news`);
    return result.data.result;
  } catch (error) {
    return false;
  }
};

const GetNewsById = async (id) => {
  try {
    const result = await Http.get(`${MainUrl}news/${id}`);
    return result.data.result;
  } catch (error) {
    return false;
  }
};

export { GetNews, GetNewsById };
