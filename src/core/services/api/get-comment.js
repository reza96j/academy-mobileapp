import Http from "../interceptor/interceptor";

const MainUrl = process.env.REACT_APP_PUBLIC_PATH;

const GetComment = async () => {
  try {
    const result = await Http.get(`${MainUrl}comments`);
    return result.data;
  } catch (error) {
    return false;
  }
};

const SendComment = async (obj) => {
  try {
    const result = await Http.post(`${MainUrl}comments/send`, obj);
    return result.data;
  } catch (error) {
    return false;
  }
};

export { GetComment, SendComment };
