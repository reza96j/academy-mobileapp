import Http from "../interceptor/interceptor";

const MainUrl = process.env.REACT_APP_PUBLIC_PATH;

const UploadImage = async (object) => {
  try {
    const result = await Http.post(`${MainUrl}upload/image`, object, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });

    return result.data.result;
  } catch (error) {
    return false;
  }
};

const UpdateEditProfile = async (userId, obj) => {
  try {
    const result = await Http.put(`${MainUrl}student/${userId}`, obj);

    return result.data.result;
  } catch (error) {
    return false;
  }
};
export { UploadImage, UpdateEditProfile };
