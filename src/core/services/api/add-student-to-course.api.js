import Http from "../interceptor/interceptor";

const MainUrl = process.env.REACT_APP_PUBLIC_PATH;

const AddStudentToCourse = async (userId, courseId) => {
  try {
    const result = await Http.post(
      `${MainUrl}course/addStudentToCourse/${userId}`,
      courseId
    );
    return result.data.result;
  } catch (error) {
    return false;
  }
};
export { AddStudentToCourse };
