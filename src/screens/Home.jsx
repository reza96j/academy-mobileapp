import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useEffect, useState } from "react";
import {
  Button,
  Dimensions,
  Image,
  ScrollView,
  Text,
  View,
} from "react-native";
import CarouselCards from "../components/carouselCards/CarouselCards";

function Home({ navigation }) {
  const WIDTH = Dimensions.get("window").width;
  const HEIGHT = Dimensions.get("window").height;
  return (
    <ScrollView
      contentContainerStyle={{ justifyContent: "center", alignItems: "center" }}
    >
      <View className=" w-full h-48 my-3">
        <CarouselCards />
      </View>
      <View className="w-screen my-3 mb-5 flex-row justify-between px-4">
        <View className="w-1/5 border h-16"></View>
        <View className="w-1/5 border h-16"></View>
        <View className="w-1/5 border h-16"></View>
        <View className="w-1/5 border h-16"></View>
      </View>
      <View>
        <Text className="text-2xl my-1 mr-4" style={{ fontFamily: "yekan" }}>
          جدید ترین ها
        </Text>
        <View className="border w-screen h-36 mb-3 pr-4">
          <Text>carousel</Text>
        </View>
      </View>
      <View>
        <Text className="text-2xl my-1 mr-4" style={{ fontFamily: "yekan" }}>
          جدید ترین ها
        </Text>
        <View className="border w-screen h-36 mb-3 pr-4">
          <Text>carousel</Text>
        </View>
      </View>
      <View>
        <Text className="text-2xl my-1 mr-4" style={{ fontFamily: "yekan" }}>
          اساتید برتر
        </Text>
        <View className="border w-screen h-36 mb-3 pr-4">
          <Text>carousel</Text>
        </View>
      </View>
    </ScrollView>
  );
}

export default Home;
