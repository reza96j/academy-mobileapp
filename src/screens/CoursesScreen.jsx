import { StatusBar } from "expo-status-bar";
import React from "react";
import { FlatList, SafeAreaView } from "react-native";
import CourseCard from "../components/CourseCard";
import CourseData from "../fakeData/FakeData";

function CoursesScreen() {
  return (
    <SafeAreaView className="px-3 mt-4">
      <StatusBar style="auto" />
      <FlatList
        data={CourseData} //------ main data ------
        renderItem={({ item }) => (
          <CourseCard
            course={item.courseName}
            price={item.price}
            teacher={item.tacher}
            cardID={item.id}
          />
        )}
        keyExtractor={(item) => item.id}
      />
    </SafeAreaView>
  );
}

export default CoursesScreen;
