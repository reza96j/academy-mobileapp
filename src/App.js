import {
  NavigationContainer,
  DarkTheme,
  DefaultTheme,
} from "@react-navigation/native";
import { useFonts } from "expo-font";
import "react-native-gesture-handler";
import React from "react";
import { useColorScheme, View } from "react-native";
import HomeTab from "./routes/HomeTab";
import DrawerNvigate from "./routes/DrawerNvigate";

function App() {
  const scheme = useColorScheme();
  const [fontsLoaded] = useFonts({
    yekan: require("./assets/font/Yekan.ttf"),
  });
  if (!fontsLoaded) {
    return null;
  }
  return (
    <>
      <NavigationContainer theme={scheme === "dark" ? DarkTheme : DefaultTheme}>
        <DrawerNvigate />
      </NavigationContainer>
    </>
  );
}

export default App;
