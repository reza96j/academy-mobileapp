import React, { Children } from "react";
import CoursesStack from "./CoursesStack";
import HomeStack from "./HomeStack";
import { Ionicons, MaterialIcons, FontAwesome } from "@expo/vector-icons";
import { useColorScheme } from "nativewind";
import CartInformation from "../screens/CartScreen";
import { TextInput, View } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import FavoriteScreen from "../screens/FavoriteScreen";
import UserScreen from "../screens/UserScreen";

const HomeTabs = createBottomTabNavigator();

function HomeTab() {
  const scheme = useColorScheme();
  return (
    <HomeTabs.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarHideOnKeyboard: true,
        tabBarActiveTintColor: "red",
        tabBarInactiveTintColor: "green",
        tabBarStyle: {
          height: 70,
          shadowRadius: 80,
          shadowOffset: 90,
          shadowOpacity: 100,
          shadowColor: "#333",
          borderTopLeftRadius: 40,
          borderTopRightRadius: 40,
        },
      }}
    >
      <HomeTabs.Screen
        name="Homes"
        component={HomeStack}
        options={{
          tabBarIcon: ({ focused }) => (
            <Ionicons
              style={{ color: focused ? "red" : "#444" }}
              name="ios-home"
              size={30}
            />
          ),
        }}
      />
      <HomeTabs.Screen
        name="profile"
        component={UserScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <FontAwesome
              style={{ color: focused ? "red" : "#444" }}
              name="user"
              size={30}
            />
          ),
        }}
      />
      <HomeTabs.Screen
        name="cart"
        component={CartInformation}
        options={{
          tabBarBadge: 8,
          tabBarIcon: ({ focused }) => (
            <View
              style={{ marginBottom: focused ? 25 : 20 }}
              className="rounded-full p-2 bg-red-400 h-20 w-20 items-center justify-center"
            >
              <MaterialIcons name="shopping-cart" size={30} color="white" />
            </View>
          ),
        }}
      />
      <HomeTabs.Screen
        name="courses"
        component={CoursesStack}
        options={{
          tabBarIcon: ({ focused }) => (
            <FontAwesome
              style={{ color: focused ? "red" : "#444" }}
              name="book"
              size={30}
            />
          ),
        }}
      />
      <HomeTabs.Screen
        name="favorite"
        component={FavoriteScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <MaterialIcons
              style={{ color: focused ? "red" : "#444" }}
              name="favorite"
              size={30}
            />
          ),
        }}
      />
    </HomeTabs.Navigator>
  );
}

export default HomeTab;
