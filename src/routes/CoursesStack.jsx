import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import CourseScreen from "../screens/CourseInformation";
import CoursesScreen from "../screens/CoursesScreen";

const CourseStack = createNativeStackNavigator();

function CoursesStack() {
  return (
    <CourseStack.Navigator screenOptions={{ headerShown: false }}>
      <CourseStack.Screen name="course" component={CoursesScreen} />
      <CourseStack.Screen name="lesson" component={CourseScreen} />
    </CourseStack.Navigator>
  );
}

export default CoursesStack;
