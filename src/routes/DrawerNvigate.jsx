import { Text, TextInput, View } from "react-native";
import React, { Component } from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { Feather } from "@expo/vector-icons";
import HomeTab from "./HomeTab";

export class DrawerNvigate extends Component {
  render() {
    const drawer = createDrawerNavigator();
    return (
      <drawer.Navigator
        screenOptions={{
          headerStyle: {
            height: 90,
            borderBottomLeftRadius: 40,
            borderBottomRightRadius: 40,
          },

          headerTitleAlign: "center",
          headerTitle: () => (
            <View className="px-3 bg-slate-200 w-60 h-9 rounded-xl ">
              <TextInput className="" editable />
            </View>
          ),
          headerRight: () => (
            <View className="mr-4">
              <Feather name="filter" size={24} color="black" />
            </View>
          ),
        }}
      >
        <drawer.Screen name="main" component={HomeTab} />
      </drawer.Navigator>
    );
  }
}

export default DrawerNvigate;
