import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "../screens/Home";
import CartInformation from "../screens/CartScreen";

const HomeStacks = createNativeStackNavigator();

function HomeStack() {
  return (
    <HomeStacks.Navigator screenOptions={{ headerShown: false }}>
      <HomeStacks.Screen name="Home" component={Home} />
    </HomeStacks.Navigator>
  );
}

export default HomeStack;
